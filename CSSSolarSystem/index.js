const toggleButton = document.getElementById('toggle');
const moon = document.getElementById('moon');
const earthSystem = document.getElementById('earth-system');
const sun = document.getElementById('sun');

const toggleAnnimation = (el) => {
	el.classList.toggle('paused-animation');
};

const toggleFullAnimation = () => {
	toggleAnnimation(moon);
	toggleAnnimation(earthSystem);
};

toggleButton.addEventListener('click', toggleFullAnimation);
sun.addEventListener('mouseover', toggleFullAnimation);
sun.addEventListener('mouseleave', toggleFullAnimation);

/*
Agregar Mercurio, Venus, Marte y todas las lunas
Boton que pare todo
Boton que pare solo las lunas
Boton que pare solo los planetas
Al apretar una luna, esta desaparece
Al apretar un planeta, este y sus lunas desaparecen
Al apretar el sol todo desaparece
*/
